using Godot;
using System;

public class TimeController : Node
{
    private ulong _StartTime = 0;
    private ulong _CurrentTime = 0;
    private ulong _TimeElapsed = 0;

    [Export]
	NodePath _LabelToDisplayNode;
	Label _LabelToDisplay;

    public override void _Ready()
    {
        _StartTime = OS.GetUnixTime();
        if(_LabelToDisplayNode != null){
            _LabelToDisplay = GetNode<Label>(_LabelToDisplayNode);
        }
    }

  public override void _Process(float delta)
  {
      _TimeElapsed += GetTimeElapsed(_StartTime);
      _StartTime = OS.GetUnixTime();
      
      if(_LabelToDisplay != null){
          _LabelToDisplay.Text = _TimeElapsed.ToString();
      }
  }

  private ulong GetTimeElapsed(ulong timeFrom){
        ulong timeElapsed = OS.GetUnixTime() - timeFrom;
        return timeElapsed;
  }
}
