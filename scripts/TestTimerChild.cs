using Godot;
using System;

public class TestTimerChild : TestTimer
{

	private string _message = "test message from testTimer";
	TestTimer tessst;

	public TestTimerChild(TestTimer messageToSend){
		messageToSend.TestProtectedMessage("test");
	}

	public override void _Ready()
	{
		tessst.TestProtectedMessage("");
	}

	public override void _Process(float delta)
	{
		GD.Print(_message);
	}

	public void TestMessage(string message){
		GD.Print("I'm recieving a message: " + message);
	}

}
