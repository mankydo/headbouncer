using Godot;
using System;

public class GameSettings : Node2D
{
[Export]
	private float _MovementSpeed = 50f;
	Vector2 _screenSize = Vector2.Zero;
	
	[Export]
	NodePath _iconToMoveNode2D;
	float _direction = 1;
	
	[Export]
	NodePath _speedSlider;
	
	[Export]
	NodePath _BackgroundColorSlider;
	[Export]
	Color _BackgroundColor = new Color("e8e6df");
	

	[Export]
	NodePath _animationNode;
	AnimationPlayer _AnimationPlayer;

	[Export]
	NodePath _fpsLabel;

	private bool _MovingState = false;

	Vector2 moveVector = new Vector2(.1f, 0);

    public BounceGameSettings Settings;

    public override void _Ready()
    {
        Settings = new BounceGameSettings();

    	Settings._MovementSpeed = 50f;
	    Settings._screenSize = GetViewportRect().Size;
        Settings._BackgroundColor = new Color("e8e6df");

		if(_BackgroundColorSlider != null ){
			Settings._BackgroundColorSlider = (VSlider)GetNode(_BackgroundColorSlider);
		}

		if(_iconToMoveNode2D != null ){
			Settings._iconToMove = (Node2D)GetNode(_iconToMoveNode2D);
		}
		GD.Print("Hello from settings!");

		if(_speedSlider != null ){
			Settings._speedSlider = (HSlider)GetNode(_speedSlider);
		}

		if(_animationNode != null){
			Settings._AnimationPlayer = (AnimationPlayer)GetNode(_animationNode);
		}
		if( _fpsLabel !=null){
			Settings._fpsText = (Label)GetNode(_fpsLabel);
		}
    }

}

public struct BounceGameSettings
{
	public float _MovementSpeed;
	public Vector2 _screenSize;
	public Color _BackgroundColor;
	public VSlider _BackgroundColorSlider;
	public Node2D _iconToMove;
	public HSlider _speedSlider;
	public AnimationPlayer _AnimationPlayer;
	public Label _fpsText;
}
