using Godot;
using System;

public class StartScene : Node2D
{
	[Export]
	private float _MovementSpeed = 50f;
	Vector2 _screenSize = Vector2.Zero;
	
	[Export]
	NodePath _iconToMoveNode;
	Node2D _iconToMove;
	float _direction = 1;
	
	[Export]
	NodePath _speedSliderNode;
	HSlider _speedSlider;
	
	[Export]
	NodePath _BackgroundColorSliderNode;
	VSlider _BackgroundColorSlider;
	[Export]
	Color _BackgroundColor = new Color("e8e6df");
	

	[Export]
	NodePath _animationNode;
	AnimationPlayer _AnimationPlayer;

	[Export]
	NodePath _fpsNode;
	RichTextLabel _fpsText;

	private bool _MovingState = false;

	Vector2 moveVector = new Vector2(.1f, 0);

	private TestTimer _testTimer;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

		_testTimer = new TestTimer();
		_testTimer.Name = "test node";
		AddChild(_testTimer);

		if(_iconToMoveNode != null ){
			_iconToMove = (Node2D)GetNode(_iconToMoveNode);
		}
		GD.Print("Hello!");

		if(_speedSliderNode != null ){
			_speedSlider = (HSlider)GetNode(_speedSliderNode);
		}
		if(_BackgroundColorSliderNode != null ){
			_BackgroundColorSlider = (VSlider)GetNode(_BackgroundColorSliderNode);
		}

		if(_animationNode != null){
			_AnimationPlayer = (AnimationPlayer)GetNode(_animationNode);
			_AnimationPlayer.Play("spin");
		}
		if( _fpsNode !=null){
			_fpsText = (RichTextLabel)GetNode(_fpsNode);
		}

		_screenSize = GetViewportRect().Size;
	}

 // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Process(float delta)
  {
	if(_MovingState){
		_AnimationPlayer.PlaybackActive = true;
		_screenSize = GetViewportRect().Size;

	  	float _MovementSpeedUserAmplified = 1;
		if( _speedSlider != null ){
			_MovementSpeedUserAmplified *= (float) _speedSlider.Value;
//			GD.Print("slider speed is : " + _speedSlider.Value);
		}

		if( _iconToMove != null) {

			if( Mathf.Max(0, Mathf.Min(_iconToMove.Position.x,_screenSize.x )) != _iconToMove.Position.x)
			{
				moveVector *= -1;
			};

			_iconToMove.Position += moveVector * _MovementSpeed * _MovementSpeedUserAmplified;
		}


//		GD.Print("position is: " +_iconToMove.Position);
//		GD.Print("position is: " + Engine.GetFramesPerSecond()) ;

		//Visual things
		if( _BackgroundColorSlider !=null ){
			VisualServer.SetDefaultClearColor(  
				_BackgroundColor.LinearInterpolate(
					Colors.Black, 
					1- (float) _BackgroundColorSlider.Value
				)
			);
		}

		if(_fpsText != null){
			_fpsText.Text = "fps is: " + Engine.GetFramesPerSecond();
		}
	} else {
		_AnimationPlayer.PlaybackActive = false;
	}
  }

  private void ToggleMovingState(){
//	  GetTree().Paused = _MovingState;
	  _MovingState = !_MovingState;
	  _testTimer.TestMessage("Hi there from start Scene!");
  }
}
