using Godot;
using System;

public class TestTimer : Node2D
{

	private string _message = "test message from testTimer";
	
	public override void _Ready()
	{
	}

	public override void _Process(float delta)
	{
		GD.Print(_message);
	}

	public void TestMessage(string message){
		GD.Print("I'm recieving a message: " + message);
	}

	protected internal void TestProtectedMessage(string message){
		GD.Print("PROTECTED: " + message);
	}

}
