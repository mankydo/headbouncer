using Godot;
using System;

public class BouncerWorker : Node, IGameWorker
{
    private bool _isPaused = false;

    public void Init(GameController gameController)
    {
        gameController.GamePausedEvent += GameIsPaused;
        GD.Print("Init on Bouncer Worker is called.");
        this.Name = "Bounce Worker";
    }

    public void GameIsPaused(bool isGamePaused)
    {
        GD.Print("Me is paused");
        _isPaused = isGamePaused;
    }

    public BouncerWorker()
    {
        GD.Print("Constructor on Bouncer Worker is called.");
    }

    public override void _Ready()
    {
        
    }

    public override void _Process(float delta)
    {
        if( !_isPaused)
        {
            GD.Print("I'm bouncing");
        }
    }
}
