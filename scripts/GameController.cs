using Godot;
using System;
using System.Collections.Generic;

public class GameController : Node
{

    public delegate void GamePaused(bool pauseState);
    public event GamePaused GamePausedEvent;

    List<IGameWorker> gameWorkers = new List<IGameWorker>();

    public override void _Ready()
    {
        GD.Print("Starting the game;");
        GD.Print("Creating a bouncing worker:");
        gameWorkers.Add(new BouncerWorker());

        gameWorkers.ForEach(p => {
            p.Init(this);
            
            // This needs some more thinking where it should occur 
            // since not all workers are/will be inherited from Node
            this.AddChild((Node)p);
            }
        );

        PauseTheGame();
    }

    public void PauseTheGame()
    {
        GamePausedEvent.Invoke(true);
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
