using Godot;
using System;

public interface IGameWorker
{
    void Init(GameController gameController);
    void GameIsPaused(bool isGamePaused);
}
